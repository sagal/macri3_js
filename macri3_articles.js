import xml2js from "https://esm.sh/xml2js?pin=v55";

const clientId = Deno.env.get("CLIENT_ID");
const clientIntegrationId = Deno.env.get("CLIENT_INTEGRATION_ID");
const clientEcommerce = JSON.parse(Deno.env.get("CLIENT_ECOMMERCE"));
const partial = Deno.env.get("PARTIAL");
const fullUpdate = Deno.env.get("FULL_UPDATE");
const wsdlArticles = Deno.env.get("WSDL_ARTICLES");
const articlesSoapAction = Deno.env.get("ARTICLES_SOAPACTION");

async function init() {
  if (fullUpdate === "true") {
    let articlesToProcess = await getAllArticlesFromMacri3Full();
    await processAllArticles(articlesToProcess);
  } else {
    let articlesToProcess = await getAllArticlesFromMacri3();
    await processAllArticles(articlesToProcess);
  }
}

async function getAllArticlesFromMacri3() {
  let lastDate = new Date();
  lastDate.setDate(lastDate.getDate() - 2);
  let response = await fetch(wsdlArticles, {
    method: "POST",
    headers: {
      SOAPAction: articlesSoapAction,
      "Content-Type": "text/xml;charset=UTF-8",
    },
    body: `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
      <soapenv:Header/>
      <soapenv:Body>
              <tem:ProductosActualizados>
              <!--Optional:--> 
              <tem:fechaDesde>${lastDate.toISOString()}</tem:fechaDesde>
                  <!--Optional:-->
                  <tem:numeroDePagina>1</tem:numeroDePagina>
                  <!--Optional:-->
                  <tem:cantidadDeRegistros>600000</tem:cantidadDeRegistros>
              </tem:ProductosActualizados>
      </soapenv:Body>
      </soapenv:Envelope>`,
  });

  try {
    response = await response.text();
    let parser = new xml2js.Parser();
    let parsedResponse = await parser.parseStringPromise(response);

    return parsedResponse["s:Envelope"]["s:Body"][0][
      "ProductosActualizadosResponse"
    ][0]["ProductosActualizadosResult"][0]["a:Producto"];
  } catch (e) {
    throw new Error(`Error parsing response from fetch: ${e.message}`);
  }
}

async function getAllArticlesFromMacri3Full() {
  let response = await fetch(wsdlArticles, {
    method: "POST",
    headers: {
      SOAPAction: articlesSoapAction,
      "Content-Type": "text/xml;charset=UTF-8",
    },
    body: `<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tem="http://tempuri.org/">
      <soapenv:Header/>
      <soapenv:Body>
              <tem:ProductosActualizados>
                  <!--Optional:-->
                  <tem:numeroDePagina>1</tem:numeroDePagina>
                  <!--Optional:-->
                  <tem:cantidadDeRegistros>600000</tem:cantidadDeRegistros>
              </tem:ProductosActualizados>
      </soapenv:Body>
      </soapenv:Envelope>`,
  });

  try {
    response = await response.text();
    let parser = new xml2js.Parser();
    let parsedResponse = await parser.parseStringPromise(response);

    return parsedResponse["s:Envelope"]["s:Body"][0][
      "ProductosActualizadosResponse"
    ][0]["ProductosActualizadosResult"][0]["a:Producto"];
  } catch (e) {
    throw new Error(`Error parsing response from fetch: ${e.message}`);
  }
}

async function processAllArticles(articlesToProcess) {
  let processedArticles = [];
  for (let article of articlesToProcess) {
    try {
      let articleSku = article["a:Codigo"][0];
      let articleName = article["a:Descripcion"][0];
      let articleDescription = article["a:Descripcion"][0];
      let articlePrice = await setPrice(
        Number(
          article["a:Stocks"][0]["a:StockPrecioPorVariante"][0][
            "a:PrecioPublico"
          ][0]
        )
      );
      let articleVariants = [];
      for (let variation of article["a:Stocks"][0][
        "a:StockPrecioPorVariante"
      ]) {
        let articleSize =
          (typeof variation["a:Talle"][0]).toLowerCase() === "object"
            ? ""
            : variation["a:Talle"][0];
        articleVariants.push({
          sku: articleSku.trim() + "-Único-" + articleSize,
          properties: [
            {
              attributes: {
                SIZE: articleSize,
                COLOR: "Único",
              },
            },
            {
              price: articlePrice,
            },
            { stock: Number(variation["a:CantidadDisponible"][0]) },
          ],
        });
      }

      let processedArticle = {
        sku: articleSku.trim(),
        client_id: clientId,
        options: {
          merge: false,
          partial: partial === "true",
        },
        integration_id: clientIntegrationId,
        ecommerce: Object.values(clientEcommerce).map((ecommerce_id) => {
          let ecommerceProps = {
            ecommerce_id: ecommerce_id,
            properties: [
              {
                name: articleName,
              },
              {
                price: {
                  value: articlePrice,
                  currency: "$",
                },
              },
              {
                description: articleDescription,
              },
            ],
            variants: articleVariants,
          };
          return ecommerceProps;
        }),
      };
      processedArticles.push(processedArticle);
    } catch (e) {
      console.log(`Unable to process article: ${e.message}`);
    }
  }
  await sagalDispatchBatch({
    products: processedArticles,
    batch_size: 100,
  });
}

async function setPrice(basePrice) {
  let price = Math.round(basePrice * 1.22);
  return price;
}

await init();
